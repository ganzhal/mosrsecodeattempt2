// var names: // Must declare the main assembly entry point before use.
void main_asm();
void input();
char *grabArray();

// Include needed headers
// #include "assign02.pio.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "pico/stdlib.h"
// #include "hardware/pio.h"
#include "hardware/gpio.h"
#include "hardware/watchdog.h"

// Set definitions
#define IS_RGBW true  // Use RGBW colour scheme
#define NUM_PIXELS 1  // 1 LED being used
#define WS2812_PIN 28 // Set the GPIO

int level = 1;                    // Variable to control health, defaults to 1
int exitGame = 0;                 // Variable to show game state
PIO pio = pio0;                   // PIO for the LED
int indexGoal;                    // Index of the char
int correctGuesses, totalCorrect; // Variables for guessing
int lives, livesLost;             // Variables for lives

// Setup arrays for the morse and the alphanumerical chars
/**
 * @brief Morse for the chars
 *
 */
char *morse[] = {"-----", ".----", "..---", "...--", "....-", ".....",
                 "-....", "--...", "---..", "----.", ".-", "-...", "-.-.", "-..", ".",
                 "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---",
                 ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--."};

/**
 * @brief Array of the associated chars
 *
 */
char *letters[] = {"0", "1", "2", "3", "4", "5",
                   "6", "7", "8", "9", "A", "B", "C", "D", "E",
                   "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
                   "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

// Initialise and setup the pin
/**
 * @brief Initialise the GPIO pin
 *
 */
void asm_gpio_init(uint pin)
{
    gpio_init(pin);
}

/**
 * @brief Set direction of pin
 *
 */
void asm_gpio_set_dir(uint pin, bool out)
{
    gpio_set_dir(pin, out);
}

/**
 * @brief Pull GPIO pin val
 *
 */
bool asm_gpio_get(uint pin)
{
    return gpio_get(pin);
}

//
/**
 * @brief Set GPIO pin val
 *
 */
void asm_gpio_put(uint pin, bool value)
{
    gpio_put(pin, value);
}

/**
 * @brief Set falling edge interrupt
 *
 */
void asm_gpio_set_irq(uint pin)
{
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_FALL | GPIO_IRQ_EDGE_RISE, true);
}

// Setup watchdog timer
void dogUpdate()
{
    watchdog_update();
}

/**
 * @brief Return a time stamp (64-bit)
 */
int get_time()
{
    return time_us_64();
}

// Setup graphics
/**
 * @brief Print welcome graphic
 *
 */
void gameGraphic()
{
    printf("\033[1;35m");
    printf(" ====================================\n");
    printf(" Assignment 2\t Done by Group 19\n");
    printf(" ====================================\n");
    printf("#      #####      #     ####    #   #\n");
    printf("#      #         # #    #   #   ##  #\n");
    printf("#      ####     #   #   ####    # # #\n");
    printf("#      #       #######  # #     #  ##\n");
    printf("#####  #####  #       # #   #   #   #\n");
    printf("\n");
    printf("#     #   ###   ####     ###   ##### \n");
    printf("##   ##  #   #  #   #   #      #     \n");
    printf("#  #  #  #   #  ####     ###   ####  \n");
    printf("#     #  #   #  # #         #  #     \n");
    printf("#     #   ###   #   #    ###   ##### \n");
    printf("=====================================\n");
    printf(" \n # INSTRUCTIONS TO PLAY #\n");
    printf(" -> An alphanumeric character will be printed to the screen\n");
    printf(" -> Your task is to enter the correct morse code equivalent\n");
    printf("    Using the GP21 button\n");
    printf(" -> For morse dot -> Press GP21 button for short duration.\n");
    printf(" -> For morse dash -> Press GP21 button for long duration.\n");
    printf(" -> If your answer is correct, you will receive a life (MAX 3 lives)\n");
    printf(" -> If your answer is incorrect, you will lose a life\n");
    printf(" -> If all of your lifes are over, the GAME'S OVER\n");
    printf(" -> If you get 5 correct sequences in a row, you proceed to the next level.\n");
    printf("\033[0;39m");
}

/**
 * @brief Print game over graphic
 *
 */
void GameOverGraphic()
{
    printf("███▀▀▀██┼███▀▀▀███┼███▀█▄█▀███┼██▀▀▀\n");
    printf("██┼┼┼┼██┼██┼┼┼┼┼██┼██┼┼┼█┼┼┼██┼██┼┼┼\n");
    printf("██┼┼┼▄▄▄┼██▄▄▄▄▄██┼██┼┼┼▀┼┼┼██┼██▀▀▀\n");
    printf("██┼┼┼┼██┼██┼┼┼┼┼██┼██┼┼┼┼┼┼┼██┼██┼┼┼\n");
    printf("███▄▄▄██┼██┼┼┼┼┼██┼██┼┼┼┼┼┼┼██┼██▄▄▄\n");
    printf("┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼\n");
    printf("███▀▀▀███┼▀███┼┼██▀┼██▀▀▀┼██▀▀▀▀██▄┼\n");
    printf("██┼┼┼┼┼██┼┼┼██┼┼██┼┼██┼┼┼┼██┼┼┼┼┼██┼\n");
    printf("██┼┼┼┼┼██┼┼┼██┼┼██┼┼██▀▀▀┼██▄▄▄▄▄▀▀┼\n");
    printf("██┼┼┼┼┼██┼┼┼██┼┼█▀┼┼██┼┼┼┼██┼┼┼┼┼██┼\n");
    printf("███▄▄▄███┼┼┼─▀█▀┼┼─┼██▄▄▄┼██┼┼┼┼┼██▄\n");
}

/**
 * @brief Print stats for the player
 *
 */
void statistics()
{
    int percentageCorrect = ((totalCorrect * 100) / (totalCorrect + livesLost));
    printf("*********\n");
    printf("Statistics: ");
    printf("Total correct guesses: %d \n", totalCorrect);
    printf("Total lives lost: %d \n", livesLost);
    printf("Percent Correct: %d %% \n", percentageCorrect);
    printf("*********\n");
}

// Setup LED pins controls for the colour scheme
/**
 * @brief Wrapper function used to call the underlying PIO
 *        function that pushes the 32-bit RGB colour value
 *        out to the LED serially using the PIO0 block. The
 *        function does not return until all of the data has
 *        been written out.
 *
 * @param pixel_grb The 32-bit colour value generated by urgb_u32()
 */
static inline void put_pixel(uint32_t pixel_grb)
{
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}

/**
 * @brief Function to generate an unsigned 32-bit composit GRB
 *        value by combining the individual 8-bit paramaters for
 *        red, green and blue together in the right order.
 *
 * @param r     The 8-bit intensity value for the red component
 * @param g     The 8-bit intensity value for the green component
 * @param b     The 8-bit intensity value for the blue component
 * @return uint32_t Returns the resulting composit 32-bit RGB value
 */
static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b)
{
    return ((uint32_t)(r) << 8) |
           ((uint32_t)(g) << 16) |
           (uint32_t)(b);
}

// Set level difficulty
/**
 * @brief Select level difficulty
 *
 */
void levelInput()
{
    printf("Please enter the level you would like to attempt:\n");
    printf("Enter: '.----' for level 1\n");
    printf("Enter: '..---' for level 2\n");
    input();
    watchdog_update(); // Update for input

    char *levelInput = grabArray(); // Take pointers to index of array
    char morsereturn[6];
    for (int i = 0; i < 6; i++) // Iterate through 
    {
        morsereturn[i] = *(levelInput + (i * 4)); // Fill array with pointers to the morse chars
    }
    char *string = morsereturn;

    if (strcmp(string, morse[1]) == 0) // Compare to list above, if same as 1, set level to 1
    {
        level = 1;
    }
    if (strcmp(string, morse[2]) == 0) // If same as 2, set level to 2
    {
        level = 2;
    }
}

// Set LED colour based on lives left
/**
 * @brief Change colour of LED based on lives left
 *
 *
 */
void livesColourChange()
{
    switch (lives)
    {
    case 3:
        // Set Green
        printf("\033[1;32m");
        put_pixel(urgb_u32(0x00, 0x7F, 0x00));
        break;
    case 2:
        // Set Orange
        printf("\033[0;33m");
        put_pixel(urgb_u32(0x2F, 0xC, 0x00));
        break;
    case 1:
        // Set Blue
        printf("\033[0;34m"); // Change value for Blue (REMOVE LATER!!!!!!!!!!!)
        put_pixel(urgb_u32(0x7F, 0x7F, 0x00));
        break;
    case 0:
        // Set Red
        printf("\033[1;31m");
        put_pixel(urgb_u32(0x7F, 0x00, 0x00));
    default:
        // Set Off
        put_pixel(urgb_u32(0x00, 0x00, 0x00));
        break;
    }
}

// Search through morse code for index
/**
 * @brief Search through morse to find index of string
 *
 * @param test string
 * @return Return index of morse or error
 */
int morseSearch(char *test)
{
    for (int i = 0; i < 35; i++)
    {
        if (!strcmp(test, morse[i]))
        {
            return i;
        }
    }
    return -1;
}

// Convert asm to pointer and test for correct
/**
 * @brief Converts an array received from assembly code to a
 *        null-terminated character pointer and determines whether the input
 *        was correct or incorrect. Prints the corresponding alphanumeric
 *        character of the morse input
 *
 * @param input Pointer to the first character of the array input from the assembly code
 * @return Returns 1 if the input was correct, 0 otherwise
 */
int convertArray(char *input)
{
    char morsereturn[6];
    for (int i = 0; i < 6; i++)
    {
        morsereturn[i] = *(input + (i * 4));
    }
    char *string = morsereturn;
    int indexOfInput = morseSearch(string);

    // If not correct, return fail
    if (indexOfInput == -1)
    {
        printf("Not found: ?\n");
        return 0;
    }
    
    // If found, return input and 1
    if (indexGoal == indexOfInput)
    {
        printf("You entered: %s\n", letters[indexOfInput]);
        return 1;
    }

    // If found but not correct, return input and 0
    else
    {
        printf("You entered: %s\n", letters[indexOfInput]);
        return 0;
    }
}

/**
 * @brief Test if correct based on board input
 *
 * @return Return boolean result
 */
int checkCorrect()
{
    int correct = convertArray(grabArray());
    if (correct == 1)
    {
        printf("Correct! Well done!\n");
       return 1;
    }
    else
    {
        printf("Incorrect! Try again!\n");
        return 0;
    }
}

// Level one control
/**
 * @brief Controls the game loop for level one and manages the player's lives
 *        It returns 1 if the player has passed the level, and -1 if the player has failed the level
 * 
 * @return Returns 1 if passed, -1 if fail
 */
int levelOne()
{
    printf("Level One!\n");
    lives = 3; // Set lives to 3
    correctGuesses = 0;
    livesColourChange(); // Start game with LED change
    while (1)
    {
        indexGoal = rand() % 36; // Set random alphanum char as target
        printf("Please enter the following Character: %s, (Hint the Morse is : %s)\n", letters[indexGoal], morse[indexGoal]);
        input();
        watchdog_update();
        if (checkCorrect() == 1)
        {
            correctGuesses++;
            totalCorrect++;
            if (lives < 3)
            {
                lives++;
                livesColourChange();
            }

            if (correctGuesses == 5)
            {
                return 1;
            }
        }
        else
        {
            lives--;
            livesLost++;
            livesColourChange();

            if (lives == 0)
            {
                return -1;
            }
        }
    }
}

// Level two control
/**
 * @brief Controls the game loop for level two and manages the player's lives
 *        It returns 1 if the player has passed the level, and -1 if the player has failed the level
 * 
 * @return Returns 1 if passed, -1 if fail
 */
int levelTwo()
{
    lives = 3;
    correctGuesses = 0;
    printf("You made it to Level two!\n");
    livesColourChange(); // indicate the gaem has begun
    while (1)
    {
        indexGoal = rand() % 36;
        printf("Please enter the following Character: %s\n", letters[indexGoal]);
        input();
        watchdog_update();
        if (checkCorrect())
        {
            correctGuesses++;
            totalCorrect++;
            if (lives < 3)
            {
                lives++;
                livesColourChange();
            }

            if (correctGuesses == 5)
            {
                return 1;
            }
        }
        else
        {
            lives--;
            livesLost++;
            livesColourChange();

            if (lives == 0)
            {
                return -1;
            }
        }
    }
}

// Extra possible levels (CHANGE THIS!!!!!!!!!!!!!)
// Level three game loop
void levelThree()
{
}
// Level four game loop
void levelFour()
{
}

/**
 * @brief Game control function
 *
 */
void controlFunction()
{
    do
    {

        // Control game state
        switch (level)
        {

        // If you fail level 1, end game
        case 1:
            if (levelOne() == -1)
            {
                exitGame = 1;
            }

            // If you pass, go to level two
            else
            {
                level++;
            }
            break;

        // Same idea for level 2
        case 2:
            if (levelTwo() == -1)
            {
                exitGame = 1;
            }
            
            // If you beat level 2, you win
            else
            {
                level++;
                printf("YOU WIN! WELL DONE! \n");
                statistics();
                return;
            }
            break;

        // Else no game has been played and call level 1
        default:
            levelOne();
        }

    } while (exitGame == 0);
    GameOverGraphic();
    statistics();
    return;
}

/**
 * @brief Main for C
 *
 */
int main()
{
    // Initialise STDIO and GPIO
    stdio_init_all();

    // Seed random num with current time
    srand(time(0));

    // Initialise PIO interface
    uint offset = pio_add_program(pio, &ws2812_program);
    ws2812_program_init(pio, 0, offset, WS2812_PIN, 800000, IS_RGBW);

    // Initialise all main in asm
    main_asm();

    // Set off
    put_pixel(urgb_u32(0x00, 0x00, 0x00));

    // Watchdog
    watchdog_enable(16777215, 0); // SET 

    // Call graphic
    gameGraphic();

    // Call level input
    levelInput();

    // Call control 
    controlFunction();

    return (0);
}